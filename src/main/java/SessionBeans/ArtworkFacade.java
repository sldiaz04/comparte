/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import entities.Artwork;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author sldia
 */
@Stateless
public class ArtworkFacade extends AbstractFacade<Artwork> {

    @PersistenceContext(unitName = "com.mda_comparte_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArtworkFacade() {
        super(Artwork.class);
    }

    public List<Artwork> getAllArtworks() {
        return em.createNamedQuery("Artwork.findAll").getResultList();
    }

}
