/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import SessionBeans.ArtworkFacade;
import entities.Artwork;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author sldia
 */
public class ArtworkCommand extends FrontCommand {

    ArtworkFacade artworkFacade;

    public ArtworkCommand() {
        try {
            this.artworkFacade = InitialContext.doLookup("java:global/comparte/ArtworkFacade");
        } catch (NamingException ex) {
            Logger.getLogger(ArtworkCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void process() {
        List<Artwork> products = artworkFacade.findAll();
        //request.setAttribute("products", products);
        forward("/artworks.jsp");
    }

}
