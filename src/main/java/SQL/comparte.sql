    -- phpMyAdmin SQL Dump
    -- version 4.7.4
    -- https://www.phpmyadmin.net/
    --
    -- Servidor: 127.0.0.1:3306
    -- Tiempo de generación: 12-03-2018 a las 19:19:58
    -- Versión del servidor: 5.7.19
    -- Versión de PHP: 7.1.9

    SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    SET AUTOCOMMIT = 0;
    START TRANSACTION;
    SET time_zone = "+00:00";


    /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
    /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
    /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
    /*!40101 SET NAMES utf8mb4 */;

    --
    -- Base de datos: `comparte`
    --

    -- --------------------------------------------------------

    --
    -- Estructura de tabla para la tabla `artworks`
    --

    DROP TABLE IF EXISTS `artworks`;
    CREATE TABLE IF NOT EXISTS `artworks` (
      `id` int(10) NOT NULL AUTO_INCREMENT,
      `author_id` int(10) NOT NULL,
      `title` varchar(20) NOT NULL,
      `type` varchar(20) NOT NULL,
      `date` date NOT NULL,
      `description` varchar(100) DEFAULT NULL,
      `image` mediumblob NOT NULL,
      `total_like` int(10) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `author_id` (`author_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    -- --------------------------------------------------------

    --
    -- Estructura de tabla para la tabla `likes`
    --

    DROP TABLE IF EXISTS `likes`;
    CREATE TABLE IF NOT EXISTS `likes` (
      `id` int(10) NOT NULL AUTO_INCREMENT,
      `artwork_id` int(10) NOT NULL,
      `user_id` int(10) NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `artwork_id` (`artwork_id`,`user_id`),
      KEY `user_id` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    -- --------------------------------------------------------

    --
    -- Estructura de tabla para la tabla `users`
    --

    DROP TABLE IF EXISTS `users`;
    CREATE TABLE IF NOT EXISTS `users` (
      `id` int(10) NOT NULL AUTO_INCREMENT,
      `name` varchar(20) NOT NULL,
      `surname` varchar(40) NOT NULL,
      `email` varchar(30) NOT NULL,
      `password` varchar(20) NOT NULL,
      `phone` int(15) DEFAULT NULL,
      `city` varchar(25) DEFAULT NULL,
      `address` varchar(30) DEFAULT NULL,
      `image_profile` blob,
      `birthday` date DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `indexusuario` (`email`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    --
    -- Restricciones para tablas volcadas
    --

    --
    -- Filtros para la tabla `artworks`
    --
    ALTER TABLE `artworks`
      ADD CONSTRAINT `artworks_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

    --
    -- Filtros para la tabla `likes`
    --
    ALTER TABLE `likes`
      ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`artwork_id`) REFERENCES `artworks` (`id`) ON DELETE CASCADE,
      ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
    COMMIT;

    /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
    /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
    /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
