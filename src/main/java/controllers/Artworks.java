/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import SessionBeans.ArtworkFacade;
import entities.Artwork;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sldia
 */
@WebServlet(name = "Products", urlPatterns = {"/Products"})
public class Artworks extends HttpServlet {

    //@EJB
    //ArtworkFacade artworkFacade;
    ArtworkFacade artworkFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //processRequest(request, response);

            artworkFacade = InitialContext.doLookup("java:global/comparte/ArtworkFacade");

            // retrieve product to edit
            if (request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
                Artwork prod = artworkFacade.find(Integer.parseInt(request.getParameter("id")));
                request.setAttribute("prod", prod);
            }
            this.loadPage(request, response);
        } catch (NamingException ex) {
            Logger.getLogger(Artworks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Artwork> products = artworkFacade.findAll();
        request.setAttribute("products", products);
        request.getRequestDispatcher("./products.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String delete = request.getParameter("delete");
        long price;
        try {
            price = Long.parseLong(request.getParameter("price"));
        } catch (NumberFormatException e) {
            price = 0;
        }

        // Delete product
        if (delete != null && delete.equals("true")) {
            Artwork producto = artworkFacade.find(Integer.parseInt(id));
            artworkFacade.remove(producto);
            this.loadPage(request, response);
            return;
        }
        // Create an object
        if (id == null || id.isEmpty()) {
            Artwork producto = new Artwork();
            producto.setTitle(name);
            producto.setDescription(description);
            artworkFacade.create(producto);

        } else {// edit the object
            Artwork producto = new Artwork(Integer.parseInt(id));
            artworkFacade.edit(producto);
        }
        // Avoid sending the form
        response.sendRedirect("Artworks");// redirects to the same servlet but as a GET method
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
