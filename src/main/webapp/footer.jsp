<%-- 
    Document   : footer
    Created on : 18-mar-2018, 13:26:41
    Author     : YonePC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="py-5 bg-dark text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2 class="mb-4 text-secondary">CompArte</h2>
                    <ul class="list-unstyled">
                        <a href="#" class="text-white">Página principal</a>
                        <br>
                        <a href="#" class="text-white">¿Quienes somos?</a>
                    </ul>
                </div>
                <div class="col-4 col-md-1 align-self-center">
                    <a href="https://www.facebook.com" target="_blank"><i class="fa fa-fw fa-facebook fa-3x text-white"></i></a>
                </div>
                <div class="col-4 col-md-1 align-self-center">
                    <a href="https://twitter.com" target="_blank"><i class="fa fa-fw fa-twitter fa-3x text-white"></i></a>
                </div>
                <div class="col-4 col-md-1 align-self-center">
                    <a href="https://www.instagram.com" target="_blank"><i class="fa fa-fw fa-instagram text-white fa-3x"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3 text-center">
                    <p>© Copyright 2018 compArte - Todos los derechos reservados.</p>
                </div>
            </div>
        </div>
    </div>
</html>
