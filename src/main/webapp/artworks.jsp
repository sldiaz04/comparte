<%-- 
    Document   : artworks
    Created on : 09-mar-2018, 18:44:22
    Author     : sldia
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <title>Artworks</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center font-italic">Obras</h1>
            <hr>

            <div class="row">
                <section class="col-md-3">
                    <form action="FrontServlet" method="POST">
                        <input type="hidden" name="command" value="ArtworkCommand"/>
                        <div><input type="hidden" name="id" value="${artwork.id}"></div>
                        <div class="form-group"><label for="name">Nombre:</label><input class="form-control" type="text" name="name" value="${artwork.name}"></div>
                        <div class="form-group"><label for="description">Descripción:</label><input class="form-control" type="text" name="description" value="${artwork.description}"></div>
                        <div class="form-group"><label for="price">Precio:</label><input class="form-control" type="number" name="price" value="${artwork.price}"></div>
                        <input class="btn btn-outline-primary btn-block" type="submit" value="Save">
                    </form>
                </section>

                <c:if test="${arworks.size() != 0}">
                    <section class="col-md-9 table-responsive">
                        <table class="table table-striped table-hover table-sm">
                            <caption>Lista de obras</caption>
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="artwork" items="${artworks}">
                                    <tr>
                                        <td>${artwork.id}</td>
                                        <td>${artwork.name}</td>
                                        <td>${artwork.description}</td>
                                        <td>${artwork.price}</td>
                                        <td class="row">
                                            <a href="./Products?id=${artwork.id}" class="btn btn-outline-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;
                                            <form action="./Products" method="POST">
                                                <input type="hidden" name="id" value="${artwork.id}">
                                                <input type="hidden" name="delete" value="true">
                                                <button type="submit" class="btn btn-outline-danger"> <i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </section>
                </div>
            </c:if>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</body>
</html>
